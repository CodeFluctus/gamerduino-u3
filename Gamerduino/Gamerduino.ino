

/*********************************************
 *          GAMERDUINO U3-PROJECT
 * Created by Juho Aalto 24.10.2019
 * 
 * Building handconsole using Arduino UNO R3
 * More about build and project you can find
 * from:
 * 
 * https://gitlab.com/CodeFluctus/gamerduino-u3/
 * 
 **********************************************/

 /*----Version 0.01----
  -Seting up 1,5'OLED-screen
  -Testing basic functionality of joystick
 */

//Include librarys for screen, graphics and SPI
#include <Adafruit_SSD1351.h>
#include <Adafruit_GFX.h>
#include <SPI.h>

//Setup's for screen
#define SCREEN_WIDTH 128
#define SCREEN_HEIGHT 128

#define SCLK_PIN 2
#define MOSI_PIN 3
#define DC_PIN 4
#define CS_PIN 5
#define RST_PIN 6 //Resets on LOW-state

//Colors for screen
#define BLACK 0x0000
#define WHITE 0xFFFF
#define RED 0xF800
#define BLUE 0x001F
#define GREEN 0x07E0

//Setup for joystick
#define VRX A0
#define VRY A1
#define SW 8

static const uint16_t PROGMEM allColors[] = {BLACK, WHITE, RED, BLUE, GREEN};


Adafruit_SSD1351 screen = Adafruit_SSD1351(SCREEN_WIDTH, SCREEN_HEIGHT, CS_PIN, DC_PIN, MOSI_PIN, SCLK_PIN, RST_PIN);

unsigned long timePassed;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.println("Setting up OLED-screen");
  screen.begin();
  timePassed = millis();
}

void loop() {
  
  Serial.print("Time passed from startup: ");
  timePassed = millis();
  Serial.println(timePassed);
  delay(1000);
  // put your main code here, to run repeatedly:
  int howManyItemsInArray = sizeof(allColors) / sizeof(allColors[0]);
  //Serial.print("Array length: ");
  //Serial.println(howManyItemsInArray);
  for(int i = 0; i < howManyItemsInArray; i++)
  {
    uint16_t sideLength = 10*(i+1);
    drawSquareMiddleScreen(sideLength,allColors[i]);
    delay(100);
  }

  //Used for reading current from joystick
  /*
  int currentAtA0 = analogRead(VRX);
  int currentAtA1 = analogRead(VRY);
  Serial.print("X-akseli: ");
  Serial.println(currentAtA0);
  Serial.print("Y-akseli: ");
  Serial.println(currentAtA1);
  Serial.println();
  delay(333);*/
}

void drawSquareMiddleScreen(uint16_t oneSideLength,uint16_t color)
{
  screen.fillScreen(BLACK);
  uint16_t midleCorrection = oneSideLength/2;
  uint16_t screenXMiddle = screen.width()/2 - midleCorrection;
  uint16_t screenYMiddle = screen.height()/2 - midleCorrection;
  screen.fillRect(screenXMiddle, screenYMiddle, oneSideLength, oneSideLength, color);
}

