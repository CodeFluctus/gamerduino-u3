# Gamerduino U3

My first public Gitlab-project. I'm going build handconsole using Arduino UNO R3-microcontroller.

# Goals for project
* Build handconsole using Arduino UNO R3-microcontroller.
* Build game(s) from scratch for builded gameconsole.

# Equipent used for project
* Arduino UNO R3-microcontroller
* Buttons
* 1,5' OLED-screen
* A lot of jumpwires
* Anything I forgot now
* Some brains.

